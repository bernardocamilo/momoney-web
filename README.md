# Momoney: Webservice Module #

An android development project done by UFRJ students from 
Computer and Information Engineering.

Developers:

* Bernardo Camilo
* Rachel Castro

Supervisor:

* Sérgio Barbosa Villas-Boas

Instructions:

* Install Rails
* Clone this project
* Move to the project folder:

$ bundle install

$ rake db:create

$ rake db:migrate

$ rails s