class CreateExpenses < ActiveRecord::Migration
  def change
    create_table :expenses do |t|
      t.integer :user_id
      t.integer :category_id
      t.integer :payment_type_id
      t.float :value
      t.integer :day
      t.integer :month
      t.integer :year
      t.string :description

      t.timestamps
    end
  end
end
