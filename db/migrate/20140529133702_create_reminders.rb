class CreateReminders < ActiveRecord::Migration
  def change
    create_table :reminders do |t|
      t.integer :user_id
      t.string :description
      t.boolean :completed, :default => false

      t.timestamps
    end
  end
end
