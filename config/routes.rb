MomoneyWeb::Application.routes.draw do

  devise_for :users
  namespace :api do
    devise_scope :user do
      post 'registrations' => 'registrations#create', :as => 'register'
      post 'sessions' => 'sessions#create', :as => 'login'
      delete 'sessions' => 'sessions#destroy', :as => 'logout'
    end
    
    get 'reminders' => 'reminders#index', :as => 'reminders'
    post 'reminders' => 'reminders#create'
    put 'reminders/:id/open' => 'reminders#open', :as => 'open_reminder'
    put 'reminders/:id/complete' => 'reminders#complete', :as => 'complete_reminder'

    get 'expenses' => 'expenses#index', :as => 'expenses'
    post 'expenses' => 'expenses#create'

    get 'categories' => 'categories#index', :as => 'categories'
    post 'categories' => 'categories#create'

    get 'payment_types' => 'payment_types#index', :as => 'payment_types'
    post 'payment_types' => 'payment_types#create'

    get 'data' => 'data#index', :as => 'data'
  end

  ActiveAdmin.routes(self)
  devise_for :admin_users, ActiveAdmin::Devise.config
end