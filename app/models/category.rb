class Category < ActiveRecord::Base
  belongs_to :user
  has_many :expenses

  attr_accessible :category_name
end
