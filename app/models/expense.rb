class Expense < ActiveRecord::Base
  has_one :user, :through => :category
  belongs_to :category
  belongs_to :payment_type

  attr_accessible :day, :month, :value, :year, :description, :category_id, :payment_type_id

  default_scope order('day ASC')
end
