class PaymentType < ActiveRecord::Base
  belongs_to :user
  has_many :expenses
  
  attr_accessible :payment_type
end
