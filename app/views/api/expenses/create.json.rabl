object false
node (:success) { true }
node (:info) { 'Expense created!' }
child :data do
  child @expense do
    attributes :id, :category_id, :day, :month, :description, :payment_type_id, :value, :year
  end
end