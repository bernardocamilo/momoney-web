object false
node (:success) { true }
node (:info) { 'ok' }
child :data do
  node (:expenses_count) { @expenses.size }
  child @expenses do
    attributes :id, :category_id, :day, :month, :description, :payment_type_id, :value, :year
  end
end