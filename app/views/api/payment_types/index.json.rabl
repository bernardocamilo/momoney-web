object false
node (:success) { true }
node (:info) { 'ok' }
child :data do
  node (:payment_types_count) { @payment_types.size }
  child @payment_types do
    attributes :id, :payment_type
  end
end