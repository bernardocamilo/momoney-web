object false
node (:success) { true }
node (:info) { 'Payment type created!' }
child :data do
  child @payment_type do
    attributes :id, :payment_type
  end
end