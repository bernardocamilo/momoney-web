object false
node (:success) { true }
node (:info) { 'Category created!' }
child :data do
  child @category do
    attributes :id, :category_name
  end
end