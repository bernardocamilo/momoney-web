object false
node (:success) { true }
node (:info) { 'ok' }
child :data do
  node (:categories_count) { @categories.size }
  child @categories do
    attributes :id, :category_name
  end
end