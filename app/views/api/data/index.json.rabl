object false
node (:success) { true }
node (:info) { 'ok' }
child :data do
  node (:expenses_count) { @expenses.size }
  child @expenses do
    attributes :id, :category_id, :day, :month, :description, :payment_type_id, :value, :year
  end
  node (:categories_count) { @categories.size }
  child @categories do
    attributes :id, :category_name
  end
  node (:payment_types_count) { @payment_types.size }
  child @payment_types do
    attributes :id, :payment_type
  end
  node (:reminders_count) { @reminders.size }
  child @reminders do
    attributes :id, :description
  end
end