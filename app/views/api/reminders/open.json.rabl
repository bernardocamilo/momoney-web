object false
node (:success) { true }
node (:info) { 'Reminder opened!' }
child :data do
  child @reminder do
    attributes :id, :description, :created_at, :completed
  end
end