object false
node (:success) { true }
node (:info) { 'ok' }
child :data do
  node (:reminders_count) { @reminders.size }
  child @reminders do
    attributes :id, :description, :created_at, :completed
  end
end