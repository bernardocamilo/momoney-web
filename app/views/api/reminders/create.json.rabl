object false
node (:success) { true }
node (:info) { 'Reminder created!' }
child :data do
  child @reminder do
    attributes :id, :description, :created_at, :completed
  end
end