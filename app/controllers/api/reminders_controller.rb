class Api::RemindersController < ApplicationController
  skip_before_filter :verify_authenticity_token,
                     :if => Proc.new { |c| c.request.format == 'application/json' }

  before_filter :authenticate_user!

  def index
    @reminders = current_user.reminders
  end

  def create
    @reminder = current_user.reminders.build(params[:reminder])
    if @reminder.save
      @reminder
    else
      render :status => :unprocessable_entity,
             :json => { :success => false,
                        :info => @reminder.errors.full_messages,
                        :data => {} }
    end
  end

  def open
    @reminder = current_user.reminders.find(params[:id])
    @reminder.open!
  rescue ActiveRecord::RecordNotFound
    render :status => 404,
           :json => { :success => false,
                      :info => 'Not Found',
                      :data => {} }
  end

  def complete
    @reminder = current_user.reminders.find(params[:id])
    @reminder.complete!
  rescue ActiveRecord::RecordNotFound
    render :status => 404,
           :json => { :success => false,
                      :info => 'Not Found',
                      :data => {} }
  end
end