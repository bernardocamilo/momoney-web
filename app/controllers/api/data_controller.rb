class Api::DataController < ApplicationController
  skip_before_filter :verify_authenticity_token,
                     :if => Proc.new { |c| c.request.format == 'application/json' }

  before_filter :authenticate_user!

  def index
    @expenses = current_user.expenses
    @categories = current_user.categories
    @payment_types = current_user.payment_types
    @reminders = current_user.reminders
  end
end