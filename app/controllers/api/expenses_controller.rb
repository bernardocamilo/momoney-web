class Api::ExpensesController < ApplicationController
  skip_before_filter :verify_authenticity_token,
                     :if => Proc.new { |c| c.request.format == 'application/json' }

  before_filter :authenticate_user!

  def index
    @expenses = current_user.expenses
  end

  def create
    @expense = current_user.expenses.build(params[:expense])
    if @expense.save
      @expense
    else
      render :status => :unprocessable_entity,
             :json => { :success => false,
                        :info => @expense.errors.full_messages,
                        :data => {} }
    end
  end
end