class Api::CategoriesController < ApplicationController
  skip_before_filter :verify_authenticity_token,
                     :if => Proc.new { |c| c.request.format == 'application/json' }

  before_filter :authenticate_user!

  def index
    @categories = current_user.categories
  end

  def create
    @category = current_user.categories.build(params[:category])
    if @category.save
      @category
    else
      render :status => :unprocessable_entity,
             :json => { :success => false,
                        :info => @category.errors.full_messages,
                        :data => {} }
    end
  end
end