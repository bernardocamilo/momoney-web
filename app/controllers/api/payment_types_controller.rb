class Api::PaymentTypesController < ApplicationController
  skip_before_filter :verify_authenticity_token,
                     :if => Proc.new { |c| c.request.format == 'application/json' }

  before_filter :authenticate_user!

  def index
    @payment_types = current_user.payment_types
  end

  def create
    @payment_type = current_user.payment_types.build(params[:payment_type])
    if @payment_type.save
      @payment_type
    else
      render :status => :unprocessable_entity,
             :json => { :success => false,
                        :info => @payment_type.errors.full_messages,
                        :data => {} }
    end
  end
end